export default {
  purge: [
    // Use *.tsx if using TypeScript
    './src/routes/**/*.{tsx,js,jsx}',
    './components/**/*.{tsx,js,jsx}'
  ],
  theme: {},
  variants: {},
  plugins: []
}
