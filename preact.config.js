import preactCliTailwind from 'preact-cli-tailwind'

export default function(config, env, helpers) {
  config = preactCliTailwind(config, env, helpers)
  return config
}
