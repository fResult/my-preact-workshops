import './style/index.scss'
import 'antd/dist/antd.css'
import 'tailwindcss/tailwind.css'
import App from './components/app.tsx'

export default App
