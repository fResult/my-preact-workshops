export type Job = {
  id: number
  position: string
  company: string
  logo: string
  isNew: boolean
  featured: boolean
  role: string
  level: string
  postedAt: string
  contract: string
  location: string
  languages: Array<string>
  tools: Array<string>
}
