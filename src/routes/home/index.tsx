import { FunctionComponent, h } from 'preact'
import PageLayout from '../../components/page-layout'

const Home: FunctionComponent = () => {
  return (
    <div className="home">
      <PageLayout>
        <div className="card w-4/12 m-4">
          <div className="card-header">
            <h4 className="text-xl font-bold m-0">Home</h4>
          </div>
          <div className="card-body">
            <p className="bg-pink-200">This is the Home component.</p>
            <p className="bg-pink-200">This is the Home component.</p>
            <p className="bg-pink-200">This is the Home component.</p>
            <p className="bg-pink-200">This is the Home component.</p>
            <p className="bg-pink-200">This is the Home component.</p>
          </div>
        </div>
        <button className="bg-blue-700 text-white font-bold py-2 px-4 rounded">
          FIND FRIEND
        </button>
      </PageLayout>
    </div>
  )
}

export default Home
