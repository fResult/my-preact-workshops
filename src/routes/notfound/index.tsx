import { FunctionalComponent, h } from 'preact'
import { Link } from 'wouter-preact'
import PageLayout from '../../components/page-layout'

const Notfound: FunctionalComponent = () => {
  return (
    <div className="notfound">
      <PageLayout>
        <h1>Error 404</h1>
        <p>That page doesn&apos;t exist.</p>
        <Link href="/">
          <a>
            <h1>Back to Home</h1>
          </a>
        </Link>
      </PageLayout>
    </div>
  )
}

export default Notfound
