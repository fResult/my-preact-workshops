import { h } from 'preact'
import { FC } from 'preact/compat'
import { useEffect, useState } from 'preact/hooks'
import { Button } from 'antd'

interface Props {
  user: string
}

const Profile: FC<any> = (props: any) => {
  const { username } = props
  const [time, setTime] = useState<number>(Date.now())
  const [count, setCount] = useState<number>(0)

  // gets called when this route is navigated to
  useEffect(() => {
    const timer = window.setInterval(() => setTime(Date.now()), 1000)

    // gets called just before navigating away from the route
    return () => {
      clearInterval(timer)
    }
  }, [])

  // update the current time
  const increment = () => {
    setCount(count + 1)
  }

  return (
    <div className="style.profile">
      <h1>Profile: {username || 'Me'}</h1>
      <p>This is the user profile for a user named {username}.</p>

      <div>Current time: {new Date(time).toLocaleString()}</div>

      <p>
        {/* @ts-ignore */}
        <Button type="primary" onClick={increment}>
          {'Click Me'}
        </Button>
        &nbsp;Clicked {count} times.
      </p>
    </div>
  )
}

export default Profile
