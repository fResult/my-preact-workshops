import { h } from 'preact'
import PageLayout from '../../../components/page-layout'
import JobCard from '../../../components/workshops/job-listing/job-card'

// import jobsData from '../../../assets/workshops/job-listing/API/data.json'
import { useEffect, useState } from 'preact/hooks'
import { Job } from '../../../types/Job'

const JobList = () => {
  const [jobs, setJobs] = useState([])
  const [selectedTags, setSelectedTags] = useState(['CSS'])

  useEffect(() => {
    fetch('../../../assets/workshops/job-listing/API/data.json')
      .then((res) => res.json())
      .then((jobsData) => {
        setJobs(jobsData)
      })
  }, [])

  const filteredJobs = jobs?.filter(filterJobsByTags)

  function filterJobsByTags({ role, level, tools, languages }: Job) {
    if (selectedTags?.length === 0) return jobs

    const tags = [role, level, ...tools, ...languages]
    // return tags.some((tag) => selectedTags?.includes(tag))
    return selectedTags.every((selectedTag) => tags.includes(selectedTag))
  }

  function handleSelectTag({ target }) {
    const tag = target.innerText
    if (selectedTags.includes(tag)) return
    setSelectedTags([...selectedTags, tag])
  }

  function handleRemoveTag(tag) {
    setSelectedTags(selectedTags.filter((selectedTag) => selectedTag !== tag))
  }

  function handleClearTags() {
    setSelectedTags([])
  }

  return (
    <div className="job-listing bg-blue-100">
      <PageLayout style={{ marginTop: -56 }}>
        {/*<h1 className="text-4xl">Job Listing Workshop</h1>*/}
        <header
          className="bg-teal-500 mb-12 -mt-2"
          style={{ marginRight: -20, marginLeft: -20 }}
        >
          <img
            className="w-full"
            src="/assets/workshops/job-listing/images/bg-header-desktop.svg"
            alt="Image  Header"
          />
        </header>
        <div className="container m-auto">
          {/*{filteredJobs.length > 0 &&}*/}
          <div
            className="flex flex-wrap -mt-20 mb-16 z-10 relative bg-white shadow-md mx-4 p-6 rounded"
            style={{ minHeight: 86 }}
          >
            {selectedTags.length > 0 &&
              selectedTags.map((tag) => (
                <span
                  onClick={() => handleRemoveTag(tag)}
                  className="cursor-pointer inline-block text-teal-500 bg-teal-100 p-2 lg:mb-0 mr-4 mb-4 rounded font-bold"
                >
                  × {tag}
                </span>
              ))}
            <button
              className="ml-auto font-bold text-gray-700"
              onClick={handleClearTags}
            >
              Clear
            </button>
          </div>

          {jobs.length === 0 ? (
            <p>Jobs are fetching...</p>
          ) : (
            filteredJobs?.map((job: Job) => (
              <JobCard
                {...{ job }}
                key={job.id}
                onSelectTag={handleSelectTag}
              />
            ))
          )}
        </div>
      </PageLayout>
    </div>
  )
}

export default JobList
