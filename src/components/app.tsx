import { h } from 'preact'
import { FC } from 'preact/compat'
import { Route, Switch } from 'wouter-preact'

import Home from '../routes/home'
import Profile from '../routes/profile'
import NotFoundPage from '../routes/notfound'
import Header from './header'
import JobList from '../routes/workshops/job-listing'

const routes = [
  { path: '/', component: Home },
  { path: '/profiles', component: Profile },
  { path: '/workshops/job-listing', component: JobList },
  { path: '/profiles/:username', component: Profile },
  { component: NotFoundPage }
]

const App: FC = () => {
  return (
    <div id="app">
      <Header />
      <Switch>
        {routes.map(({ path, component: Component }) => {
          return path ? (
            <Route key={path} path={path}>
              {(params) => <Component {...params} />}
            </Route>
          ) : (
            <Route>
              <Component />
            </Route>
          )
        })}
      </Switch>
    </div>
  )
}

export default App
