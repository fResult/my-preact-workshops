import { ComponentChild, h } from 'preact'

type Props = {
  children: ComponentChild
  style?: {}
}

const PageLayout = ({ style, children }: Props) => {
  return (
    <div className="page-layout" style={style}>
      {children}
    </div>
  )
}

export default PageLayout
