import { FunctionComponent, h } from 'preact'
import { Job } from '../../../../types/Job'

type Props = {
  job: Job
  onSelectTag?: (e: MouseEvent) => void
}

const JobCard: FunctionComponent<Props> = ({ job, onSelectTag }: Props) => {
  const {
    isNew,
    company,
    featured,
    postedAt,
    contract,
    location,
    position,
    languages,
    tools,
    role,
    level,
    logo
  } = job

  const tags = [role, level, ...languages, ...tools]

  return (
    <div
      className={`flex flex-col lg:flex-row bg-white shadow-lg mx-4 my-12 lg:my-8 p-6 rounded ${
        featured && 'border-l-4 border-teal-500'
      }`}
    >
      <div>
        <img
          className="-mt-16 mb-4 w-20 h-20 lg:my-0 lg:h-24 lg:w-24"
          src={`/assets/${logo.replace(
            'images',
            'workshops/job-listing/images'
          )}`}
          alt={company}
        />
      </div>

      <div className="ml-4 flex-col justify-between flex">
        <h3 className="font-bold text-base text-teal-500">
          {company}
          {isNew && (
            <span className="text-sm bg-teal-500 text-teal-100 py-1 px-2 m-2 font-bold rounded-full uppercase">
              New!
            </span>
          )}
          {featured && (
            <span className="text-sm bg-gray-800 text-white py-1 px-2 font-bold rounded-full uppercase">
              Featured
            </span>
          )}
        </h3>
        <h2 className="font-bold text-xl my-2 lg:my-0">{position}</h2>
        <p className="text-gray-700">
          {postedAt} &middot; {contract} &middot; {location}
        </p>
      </div>

      <div className="divider lg:hidden" />

      <div className="flex flex-wrap items-center lg:ml-auto lg:justify-center">
        {tags.length > 0 &&
          tags.map((tag, idx) => (
            <span
              className="cursor-pointer text-teal-500 bg-teal-100 p-2 mr-4 mb-4 font-bold rounded lg:mb-0"
              key={idx}
              onClick={onSelectTag}
            >
              {tag}
            </span>
          ))}
      </div>
    </div>
  )
}

export default JobCard
