import { FunctionComponent, h } from 'preact'
import { Link } from 'wouter-preact'
import { Menu } from 'antd'

const { SubMenu } = Menu

const Header: FunctionComponent = () => {
  const links = [
    { name: 'Home', path: '/' },
    {
      name: 'Workshops',
      path: '/workshops',
      subLinks: [
        { name: 'Job Listing', subPath: '/job-listing' },
        { name: 'Nope', subPath: '/nope' }
      ]
    },
    {
      name: 'Profiles',
      path: '/profiles',
      subLinks: [
        { name: 'Korn', subPath: '/korn' },
        { name: 'John', subPath: '/john' }
      ]
    }
  ]

  return (
    <div className="header-comp">
      <header className="header">
        <h3 className="bg-pink-400 text-lg">Preact App</h3>
        <Menu {...{ mode: 'horizontal' }} className="bg-transparent">
          {links.map(({ name, path, subLinks = [] }) => {
            return subLinks.length === 0 ? (
              <Menu.Item key={path}>
                <Link href={path}>
                  <a>{name}</a>
                </Link>
              </Menu.Item>
            ) : (
              <SubMenu key={path} title={name}>
                {subLinks.map(({ name, subPath }) => (
                  <Menu.Item key={path + subPath}>
                    <Link href={path + subPath}>
                      <a>{name}</a>
                    </Link>
                  </Menu.Item>
                ))}
              </SubMenu>
            )
          })}
        </Menu>
      </header>
    </div>
  )
}

export default Header
